import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'micro-sentry-test';

  constructor() {
    throw new Error("This is from micro sentry")
  }
}
